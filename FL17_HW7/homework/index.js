function getAge(birthday) {
    let today = new Date(),
        birthDate = new Date(birthday),
        age = today.getFullYear() - birthDate.getFullYear(),
        month = today.getMonth() - birthDate.getMonth()
    if ( month < 0 || month === 0 && today.getDate() < birthDate.getDate()) {
        age--;
    }
    return age;
}

function getWeekDay(dateObj) {
    let newDateFormat = new Intl.DateTimeFormat('en-US', { weekday: 'long' });
        return newDateFormat.format(dateObj);
}

function getAmountDaysToNewYear(dateObj) {
    let ms = 1000,
        sec = 60,
        min = 60,
        h = 24,
        oneDay = ms * sec * min * h,
        newYearDate = new Date('2022, January, 1');
        return Math.floor((newYearDate - dateObj) / oneDay);
}

function getProgrammersDay(year) {
    let newFormat = new Intl.DateTimeFormat('en-US', { weekday: 'long' }),
        comYear = new Date(`${year} September 13`),
        leapYear = new Date(`${year} September 12`)
    if (year % 4 === 0 && year % 100 > 0 || year %400 === 0) {
        return `12 Sep, ${year} (${newFormat.format(leapYear)})`;
    } else {
        return `13 Sep, ${year} (${newFormat.format(comYear)})`;
    }
}

let date = new Date(),
    weekday = date.getDay(),
    day = '';

function howFarIs(farDay){
  let getNumberOfDay = whatDayIs(farDay.toLowerCase()),
      daysLeft = getNumberOfDay - weekday;
      
      if(daysLeft === 0) {
        console.log(`Hey, today is ${farDay}`);
      } else if(daysLeft > 0) {
        console.log(`It's ${daysLeft} day(s) left till ${farDay}.`)
      } else{
      
        console.log(`It's ${daysLeft + 7} day(s) left till ${farDay}.`)
      }
}

function whatDayIs(day){
  switch (day) {
    case 'sunday':
      return day === 0;
    case 'monday':
      return day === 1;
    case 'tuesday':
      return day === 2;
    case 'wednesday':
      return day === 3;
    case 'thursday':
      return day === 4;
    case 'friday':
      return day === 5;
    case 'saturday':
      return day === 6;
  }
}

function isValidIdentifier(argument) {
	return 	/(^[^\d])(?=.*?[a-zA-Z])(?=.*[0-9a-zA-Z])(?=.*[$_])/.test(argument);
}

function capitalize(text) {
	return text.replace(/\b\w/g, c => c.toUpperCase());
}

function isValidAudioFile(file) {
	return 	/(^[a-zA-Z]{1,})+(.mp3|.flac|.alac|.aac)$/.test(file);
}

function getHexadecimalColors(inputColor) {
	return inputColor.match(/#([a-f0-9]{3}){1,2}\b/gi) || [];
}

function isValidPassword(pass) {
	return 	/(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[0-9a-zA-Z]{8,}/.test(pass);
}

function addThousandsSeparators(numbers) {
	return numbers.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

function getAllUrlsFromText(textToUrl) {
	return textToUrl.match(/(https?:\/\/(?:www|(?!www))[^\s]+[^\s]{2,}|www[^\s]+[^\s]{2,})/gi) || [];
}
