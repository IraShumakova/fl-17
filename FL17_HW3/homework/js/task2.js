// Your code goes here
let maxNumber = 8,
    roundOnePocket = Math.floor(Math.random()*maxNumber),
    total = 0,
    prizeScale = 2,
    numberScale = 4,
    maxAttempts = 3,
    prizes = {
        0: 100,
        1: 50,
        2: 25
}

function validate(counter) {
            
        if (counter > maxAttempts-1) {
            let restart = confirm(`Thank you for your participation. Your prize is: ${total}$.\n
            Do you want to play again?`);
            if (restart) {
                validate(0)
            } 
            return;
        } 
        let roundOne = prompt(`\nChoose a roulette pocket number from 0 to ${maxNumber}
        \nAttempts left: ${maxAttempts - counter}\nTotal Prize: ${total}$
        \nPossible prize on current attempt: ${prizes[counter]}$`,'')
        if (roundOne === '') {
            counter++
            return validate(counter);
        }
        roundOne = Number(roundOne)
        if (roundOne === roundOnePocket) {
            total += prizes[counter];
            let continueGame = confirm(`Congratulation, you won! Your prize is: ${prizes[counter]}$.\n
            Do you want to continue?`);
            if (continueGame) {
                maxNumber += numberScale;
                prizes[0] = prizes[0]*prizeScale;
                prizes[1] = prizes[1]*prizeScale;
                prizes[2] = prizes[2]*prizeScale;
                roundOnePocket = Math.floor(Math.random()*maxNumber);
                console.log(prizes)
                validate(0);
            }
            return;
        }
        counter++;
        maxNumber = 8;
        prizes[0] = 100;
        prizes[1] = 50;
        prizes[2] = 25;
        return validate(counter);
    } 
    
let start = confirm('Do you want to play a game?', '');
    if(start === true) {
        validate(0);
    } else {
        alert('You did not become a billionaire, but can.')
    }