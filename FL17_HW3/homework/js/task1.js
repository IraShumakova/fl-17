// Your code goes here
let errorMess = 'Invalid input data',
    minamount = 1000,
    forfixed = 2,
    perlimit = 100

let initialAmount = +prompt('Initial amount','');
if (initialAmount >= minamount) {
    let yearsQuantity = +prompt('Input number of years');
    if (yearsQuantity > 1 && Number.isInteger(yearsQuantity)) {
        let percentageOfYear = +prompt(' Input percentage of a year');
        if (percentageOfYear > 0 && percentageOfYear <= perlimit) {
            let totalAmount = initialAmount * Math.pow(1 +percentageOfYear/perlimit, yearsQuantity);
            let profit = totalAmount - initialAmount;
            alert(`\n
                   Initial amount: ${initialAmount}
                   Number of Years: ${yearsQuantity}
                   Percentage of Year: ${percentageOfYear}\n\n
                   Total profit: ${profit.toFixed(forfixed)}
                   Total amount: ${totalAmount.toFixed(forfixed)}`)
        } else { 
            alert(errorMess) 
        }
    } else { 
        alert(errorMess)
    }
} else { 
    alert(errorMess) 
}