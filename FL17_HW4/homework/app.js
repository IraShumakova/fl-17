function reverseNumber(num) {
    if (num === "") {
        return "";  
    } else {    
        return reverseNumber(num.substring(1)) + num.charAt(0);
    }
}

function forEach(arr, func) {
    for(let i of arr) {
        func(i);
    }
}

function map(arr, func) {
    let newData = [];
    for(let i of arr) {
        newData.push(func(i));
    }
    return newData
}

function filter(arr, func) {
    let newData = [];
    for(let i of arr) {
        if (func(i)) {
            newData.push(i);
       }
    }
    return newData
}

function getAdultAppleLovers(data) {
    let appleLovers = []
    for(let i = 0; i < data.length; i++){
        if (data[i].age >= two && data[i].favoriteFruit === 'apple') {
            appleLovers.push(data[i].name)
        }
    }
    return appleLovers
}


function getKeys(obj) {
    let keys = [];
    for (let key in obj) {
        if (key) {
            keys.push(key);
        }
    }
}

function getValues(obj) {
    let values = [];
    for (let value in obj) {
        if (value) {
            values.push(value);
        }
    }
}

function showFormattedDate(dateObj) {
    let year = { year: 'numeric' },
        day = { day: 'numeric' },
        month = { month: 'short' },
        transformDate = d => dateObj.toLocaleDateString('en-US', d)

    return 'It is '+ transformDate(day) +' of '+ transformDate(month)+ ', ' + transformDate(year)
}