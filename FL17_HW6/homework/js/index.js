function visitLink(path) {
	localStorage.setItem(path, +localStorage.getItem(path) + 1)
}

function viewResults() {
	let storageKeys = Object.keys(localStorage);
    let ul = document.createElement('ul');
    for (let path of storageKeys) {
        let li = document.createElement('li');  
        li.innerHTML = 'You visited ' + path + ' ' + localStorage.getItem(path) + ' times(s)';
        ul.appendChild(li);
    }
    document.getElementById('content').append(ul);
    localStorage.clear();
}
