// Your code goes here
function isEquals (numOne, numTwo) {
    return numOne === numTwo;
}    

function isBigger (numOne, numTwo) {
    return Boolean(numOne > numTwo);
}

function storeNames (...names) {
    return names;
}

function getDifference (numOne, numTwo) {
    let result = numOne - numTwo
    if (result >= 0) {
        return result
    } else {
        return numTwo-numOne;
    }
}

function negativeCount (num) {
    let count = 0;
    num.forEach(element => {
        if (element <0) {
            count++;
        }
    });
    return count;
}

function letterCount (word, letter) {
    let letterCount = 0;
    word.split('').forEach(element => {
        if (element === letter) {
           letterCount++; 
        }
    })
    return letterCount;
}

function countPoints (scores) {
    let count = 0;
    let winScores = 3;
    scores.forEach(element => {
        let [x, y] = element.split(':');
        x = parseInt(x);
        y = parseInt(y); 
        if (x === y) {
            count++;
        } else if (x > y) {
            count +=winScores;
        } else {
            count;
        }
    })
    return count; 
}